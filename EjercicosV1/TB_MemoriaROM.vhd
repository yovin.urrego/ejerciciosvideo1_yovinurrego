LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TB_MemoriaROM IS
END TB_MemoriaROM;
 
ARCHITECTURE behavior OF TB_MemoriaROM IS 

COMPONENT MemoriaROM
	PORT(
		salida : OUT  std_logic_vector(1 downto 0);
		direccion : IN  std_logic_vector(1 downto 0)
       );
    END COMPONENT;

signal direccion : std_logic_vector(1 downto 0) := (others => '0');
signal salida : std_logic_vector(1 downto 0); 
 
BEGIN
uut: MemoriaROM PORT MAP (
          salida => salida,
          direccion => direccion
        );
stim_proc: process
begin	
	wait for 100 ns;	
	direccion <= "01";
	wait for 100 ns;
	direccion <= "00";
	wait for 100 ns;
	direccion <= "11";
	wait for 100 ns;
	direccion <= "10";
   wait;
end process;
END;
